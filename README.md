# FlatParsing_NoServer

Script that parses InBerlinWohnen.de for new flats, that don't require WBS, and then publishes them to telegram channel IBW_NoWBS.

##
The script parses the webpage InBerlinWohnen.de, all the output is printed in the log.txt. When a new flat has been found, than the script sends it over to a telegram channel InBerlinWohnen_NoWBS. 

The log file is getting cleaned up every now and then, and the list of objects saved in the program as well.

This version doesn't include a simple webserver to print the log.txt over a webbrowser.
