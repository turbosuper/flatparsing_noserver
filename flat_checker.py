#1. Get flats: create a list with flats
#2. Check for: WBS
#3. Get the address, number of room, area, price
#TODO :get link
#4. Process the data: create a class object, 
#   calculate distance to NK, send
#5. Sending over to: website? Telegram?
#6. Put in cloud

import requests
from bs4 import BeautifulSoup
import sys
import random
import time
from datetime import datetime
from datetime import date
from contextlib import redirect_stdout
from http.server import HTTPServer, BaseHTTPRequestHandler
#import telegram
#from config import telegram_token_news

class websiteHTTP(BaseHTTPRequestHandler):

    def do_GET(self):
        self.protocol_version = 'HTTP/1.1'
        self.send_response(200, 'OK')
        self.send_header('Content-type', 'text.html')
        self.end_headers()
        publish_str = wrapTextFileIntoHTML("log.txt")
        self.wfile.write(bytes(publish_str,"utf-8"))
        #self.wfile.write(bytes("<!DOCTYPE html><html><head><title>IBW_NoWBSbot</title></head><body><h1>Bot running</h1>all logs in log.txt</body></html>","utf-8"))
    
    def do_POST(self):
        self.protocol_version = 'HTTP/1.1'
        self.send_response(200, 'OK')
        self.send_header('Content-type', 'text.html')
        self.end_headers()
        self.wfile.write(bytes("<!DOCTYPE html><html><head><title>IBW_NoWBSbot</title></head><body><h1>Bot running</h1>all logs in log.txt</body></html>","utf-8"))


class Flat:
    address =""
    rooms = 0
    size = 0
    price = 0
    def __init__(self, id, wbs, flat_link):
        self.id = id
        self.wbs = wbs
        self.flat_link = flat_link

    def __str__(self):
        wbs_req = ""
        if self.wbs == 0:
            wbs_req = "No WBS needed"
        else:
            wbs_req = "WBS needed"
        return f"id: {self.id}, rooms: {self.rooms}, size: {self.size}, price: {self.price}, {wbs_req}, Address: {self.address}, Link: {self.flat_link}"

def wrapTextFileIntoHTML(filename):
    whole = ""
    file_contents = ""
    wrapper_front = """<html> <head> <title>IBW_NoWBS_bot</title> </head><body><h1>Log.txt file:</h1>"""
    wrapper_back = "</p></body></html>"
    with open(filename) as f:
        lines_from_file = f.readlines()
    f.close()
    #i=0
    #while(i < (len(lines_from_file))):
    for i in range(len(lines_from_file)):
        file_contents = file_contents+ '<br>' + lines_from_file[i]        
        i+=1

    whole = wrapper_front+file_contents+wrapper_back
    return whole

def extract_data_from_title(flat_title_string, flat_object):
    k=0;
    for chara in flat_title_string:
        if chara != "Z":
            k += 1
        else:
            break
    flat_rooms = flat_title_string[:k]
    flat_rooms= flat_rooms.replace(",",".")
    #print("flat rooms is now:", float(flat_rooms))
    flat_title_string = flat_title_string[k+8:]
    flat_data_string = flat_title_string.split(',',1)
    flat_size = flat_data_string[0] + "." 
    flat_data_string.pop(0)
    flat_data_string = ''.join(flat_data_string[0])
    flat_data_string = flat_data_string.split(',',1)
    flat_size = flat_size + flat_data_string[0]
    flat_data_string.pop(0)
    flat_data_string = ''.join(flat_data_string[:])
    flat_data_string = flat_data_string.split(',')
    flat_price= flat_data_string[0]+","+str(flat_data_string[1])[0]+str(flat_data_string[1])[1]
    left_over_string = ''.join(flat_data_string)
    left_over_string = left_over_string.split("|")
    flat_address= left_over_string[1].strip()
    #print("Rooms:", flat_rooms, ", Size:", flat_size, ", Price:", flat_price, ", Address:", flat_address)

    #prepare strings for class atributes
    flat_price = flat_price[1:]
    flat_price = flat_price.replace(".","") #prices over 999 have an extra dot that needs to be removed
    flat_price = flat_price.replace(",",".")
    flat_size = flat_size.replace(",",".")
    flat_size = flat_size[0:-2]

    #add the extracted data to the object
    flat_object.address = flat_address
    flat_object.rooms = float(flat_rooms)
    flat_object.price = float(flat_price)
    flat_object.size = float(flat_size)

def extract_number(some_string):
    num = ''
    for character in some_string:
        if character.isdigit():
            num = num + character
    return int(num)

def check_and_append_flat(flat_object, flats_tuple, num_appended, first_round_of_parsing_flag):
    tele_post_preambule = "https://api.telegram.org/bot6186163523:AAHIb1KAQZY3T-BkzJauz8bwRCt_1ir425A/sendMessage?chat_id=@InBerlinWohnen_NoWBS&text="
    found_flag = 0

    i = 0
    while( i < len(flats_tuple)):
        #print("**********i is now:", i)
        if  flats_tuple[i].id == (flat_object.id):
            #print(f"comparing old object to new. tuple[{i}] has id {flats_tuple[i].id}, and the object hat the id: {flat_object.id}")
            found_flag = 1
            break
        i +=1
    
    if found_flag == 0:
            flats_tuple.append(flat_object)
            num_appended +=1
            with open('log.txt', 'a') as f:
                with redirect_stdout(f):
                    print(f"NEW FLAT! Flat with id:{flat_object.id}, and price: {flat_object.price}, WBS: {flat_object.wbs} (1- required, 0 - not req) succesfully added to the list.")
            #print(flat_object)
            if ((first_round_of_parsing_flag != 1) and (flat_object.wbs == 0)):
                try:
                    requests.post(tele_post_preambule+"New Flat!\nRooms: "+str(flat_object.rooms)+", size: "+str(flat_object.size) + "m2, price: " + str(flat_object.price) + "eur, address: " + str(flat_object.address)+"\n"+str(flat_object.flat_link))
                except:
                    with open('log.txt', 'a') as f:
                        with redirect_stdout(f):
                            print("ERROR! Sending to telegram failed! Trying again...")
                    time.sleep(10)
                    try: 
                        requests.post(tele_post_preambule+"New Flat!\nRooms: "+str(flat_object.rooms)+", size: "+str(flat_object.size) + "m2, price: " + str(flat_object.price) + "eur, address: " + str(flat_object.address)+"\n"+str(flat_object.flat_link))
                    except:
                        with open('log.txt', 'a') as f:
                            with redirect_stdout(f):
                                print("ERROR! Sending to telegram failed! Trying again...")
                        time.sleep(10)
                        try:
                            requests.post(tele_post_preambule+"New Flat!\nRooms: "+str(flat_object.rooms)+", size: "+str(flat_object.size) + "m2, price: " + str(flat_object.price) + "eur, address: " + str(flat_object.address)+"\n"+str(flat_object.flat_link))
                        except:
                            with open('log.txt', 'a') as f:
                                with redirect_stdout(f):
                                    print(f"ERROR! Sending to telegram failed!\nFlat with id:{flat_object.id}, and price: {flat_object.price}, WBS: {flat_object.wbs} (1- required, 0 - not req) NOT added to the list!")

    return num_appended


#function that parses the webpage and makes a tuple with all flats
def parse_webpage(flats_tuple, new_flats, first_round_of_parsing_flag):
    appended = 0
    total_flats = 0
    URL = "https://inberlinwohnen.de/wohnungsfinder/"
    #try 3 times the parsing:                
    try:
        page = requests.get(URL)
    except:
        with open('log.txt', 'a') as f:
            with redirect_stdout(f):
                print("ERROR! Parsing failed for the first time. Trying again...")
        time.sleep(10)
        try: 
            page = requests.get(URL)
        except:
            with open('log.txt', 'a') as f:
                with redirect_stdout(f):
                    print("ERROR! Parsing failed for the second time. Trying again...")
            time.sleep(10)
            try:
                page = requests.get(URL)
            except:
                with open('log.txt', 'a') as f:
                    with redirect_stdout(f):
                        print(f"ERROR! Parsing failed 3 times. Trying to return for a next loop.")
                return
 
    soup = BeautifulSoup(page.content, "html.parser")
    results = soup.find(id="_tb_relevant_results")
    #print(results.prettify())
    all_flats = results.find_all(class_ = "tb-merkflat ipg")

    for flat_element in all_flats:
        
        wbs_flag = 0
        #print("\nNew element:")
        
        #get the id of the flat
        id_tag = flat_element.find('h3')
        id_attribute = id_tag['id']
        flat_id = extract_number(id_attribute)
        #print("Flat id:",flat_id)
        
        #check for WBS
        wbs_icon = flat_element.find(class_ = "icon wbs nohand2")
        if wbs_icon:
        #    print("WBS needed")
            wbs_flag = 1
        else:
        #   print("no WBS needed")
            wbs_flag = 0
        
        #get essential info
        flat_title = flat_element.find(id)
        flat_title_string = flat_title.text
        
        #get link
        flat_link = flat_element.find(class_ = "org-but")
        flat_link = flat_link.get('href')
        #fix the link - might have whitespaces
        flat_link = (str(flat_link)).replace(' ','%20')
        flat_link = (str(flat_link)).replace(' ','%20')
        flat_link = "https://inberlinwohnen.de" + flat_link

        #create an object
        flat_obj = "flat_{}".format(flat_id);
        flat_obj = Flat(flat_id, wbs_flag, flat_link)
        extract_data_from_title(flat_title_string, flat_obj)
        total_flats += 1

        appended = check_and_append_flat(flat_obj, flats_tuple, appended, first_round_of_parsing_flag)
        new_flats.append(flat_obj)
       # print(f"Testing tha appending to new flats. new flats len:{len(new_flats)}, object id to append:{flat_obj.id}")

    with open('log.txt', 'a') as f:
        with redirect_stdout(f):
            print(f"In this round of parsing I appended {appended} new flats. The parsed list is {len(new_flats)} elements long.")
    return total_flats

def empty_list(just_parsed_flats, all_saved_flats):
    found_flag = 0
    index_to_remove = []
    if len(just_parsed_flats) != len(all_saved_flats):
    
        with open('log.txt', 'a') as f:
                with redirect_stdout(f):
                    print(f"Emptying. Length of all_flats:{len(all_saved_flats)}, recently parsed are {len(just_parsed_flats)} long") #, and the found_flag is {found_flag}")
        for i in range(len(all_saved_flats)):                              #take one entry from the saved list
            found_flag = 0                                                 #clear the found flag - the    
            for k in range(len(just_parsed_flats)):                        #check if entry exists in currently parsed    
                if ((all_saved_flats[i].id) == (just_parsed_flats[k].id)): #found it! do nothing
                    found_flag = 1;
                    break;
            if (found_flag == 0):      #not found. Remove it
                with open('log.txt', 'a') as f:
                    with redirect_stdout(f):
                        print(f"The flat with the id {all_saved_flats[i].id}, and the price {all_saved_flats[i].price} has not been found.")
                index_to_remove.append(i)
            #else:
            #    print(f"The flat with the price {all_saved_flats[i].price} eur is still on the webpage. I'm not deleting it. the i is now{i}")

        #remove the entries with the save indexes
        for m in range(len(index_to_remove)):
            #print(f"Saved {len(index_to_remove)} items to remove. Removing, the f{m}'th one, meaning the flat on the place {index_to_remove[m]} with the price {all_saved_flats[(index_to_remove[m])].price} eur")
            all_saved_flats.pop(index_to_remove[m])
        with open('log.txt', 'a') as f:
                with redirect_stdout(f):
                    print(f"Entry removed. The all_saved_flats has now {len(all_saved_flats)} elements.")


def main():
    HOST = "127.0.0.1"
    PORT = 9999
    #server = HTTPServer((HOST, PORT), websiteHTTP)
    #server.serve_forever()
    print("Running...All other output forwarded to log.txt")
    now = datetime.now()
    today = date.today()
    current_time = now.strftime("%H:%M:%S")
    time_for_log = str(today) + " on " + str(current_time)
    welcome_msg = "Program started on "+ time_for_log
    with open('log.txt', 'w') as f:
        with redirect_stdout(f):
            print(welcome_msg)
    
    #list with all flats (as objects of a class Flat)
    all_flat_objects = []
    empty_list_ctr = 0
    delete_log_file_ctr = 0
    new_flats = [] 
    first_round_of_parsing_flag = 1

    while(1):
        #check for new flats, and update the tuple with new flats found
        #print("\nParsing....")
        flats_found = parse_webpage(all_flat_objects, new_flats, first_round_of_parsing_flag)
        with open('log.txt', 'a') as f:
                with redirect_stdout(f):
                    print(f"Total flats on webpage now: {flats_found}, total flats saved: {len(all_flat_objects)}.")
        first_round_of_parsing_flag = 0             #first parsing done
        #for i in range(len(all_flat_objects)):
        #    print("flat:", i+1, all_flat_objects[i])
        
        #every 10 min check if list doesn't have to be purged from old entries
        if(empty_list_ctr >= 1):
            #print("\n10 minutes passed!")
            empty_list(new_flats,all_flat_objects)
            empty_list_ctr = 0

        empty_list_ctr +=1 
        delete_log_file_ctr +=1 
        
        #every now and then clean the log file 
        if delete_log_file_ctr >= 100:
            delete_log_file_ctr = 0
            with open('log.txt', 'w') as f:
                    with redirect_stdout(f):
                        print("Whole log file cleaned on", time_for_log)
            

        #print(f"DBG:counter of loops: {empty_list_ctr}, all_flat_objecst list has {len(all_flat_objects)} elements, and there are {len(new_flats)} elemnts in new_parsed_flats.")
        new_flats.clear()
        #print(f"Clearing new flats list... There are now {len(new_flats)} elemnts in new_parsed_flats.")

        time.sleep(60)

    #server.server_close()

if __name__ == "__main__":
    main()